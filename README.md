# audit-parse

A few small ruby scripts to help with reading your Northeastern degree audit. (WIP)

First, install bundler with `gem install bundler`.

Then, run `bundle install` to install required dependencies.

`./parse.rb` will ask you if you want to download a new audit, and then reads the audit and parses out some useful information.

Let me know if you encounter any errors running this program so I can fix them!

Pull requests are welcome and opening issues is encouraged!
