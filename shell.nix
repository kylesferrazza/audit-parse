{ pkgs ? import <nixpkgs> {} }:
let
  ruby = pkgs.ruby_2_7;
  gem_setup = ''
    mkdir -p .nix-gems
    export GEM_HOME=$PWD/.nix-gems
    export GEM_PATH=$GEM_HOME
    export PATH=$GEM_HOME/bin:$PATH
    gem install --conservative bundler
  '';
in pkgs.mkShell {
  name = "audit-parse";
  buildInputs = [
    ruby.devEnv
  ] ++ (with pkgs; [
    solargraph
    bundler
    zlib
    chromium
    chromedriver
  ]);

  shellHook = ''
    ${gem_setup}
  '';
}
