#!/usr/bin/env ruby

require "nokogiri"
require "colorize"

def divider
  puts "-----------------------------------------------"
end

def section(title)
  divider
  puts title
  divider
end

download = true
if !File.file?("audit.html")
  puts "'audit.html' does not exist. Let's download a new audit."
else
  puts "'audit.html' exists. You can use it, or download a new audit."
  valid_input = false
  while !valid_input
    printf "Would you like to download a new audit [Y/n]? "
    choice = gets.chomp
    if choice.downcase == "y" || choice.empty?
      valid_input = true
    elsif choice.downcase == "n"
      download = false
      valid_input = true
    else
      puts "Invalid selection. Please choose Y or N."
    end
  end
end

if download
  require "./get.rb"
end

begin
  $file = File.open("audit.html", "r")
rescue
  puts "Cannot read audit.html."
  exit
end

section "Information"
page = Nokogiri::HTML($file)
pre = page.css("pre")
headers = pre.css("font")

gen_date = headers[0].inner_html.split[1]
gen_time = headers[0].inner_html.split[3]
first_name = headers[1].inner_html.split[1]
last_name = headers[1].inner_html.split[0]
grad = headers[1].inner_html.split[4]
nuid = headers[0].inner_html.split.last
program = headers[2].inner_html.split[2]

puts "Audit generated on #{gen_date} at #{gen_time}."
puts "Name: #{first_name} #{last_name}."
puts "NUID: #{nuid}"
puts "Program: #{program}"
puts "Expected Graduation: #{grad}."

section "Overview"
apt = page.css(".auditPreviewText")
for item in apt
  inner = item.inner_html
  if inner.start_with? "IP-" or inner.start_with? "NO"
    puts item.inner_html.red
  elsif inner.start_with? "IP" or inner.start_with? "OK"
    puts item.inner_html.green
  end
end
