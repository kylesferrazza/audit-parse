#!/usr/bin/env ruby

require "selenium-webdriver"
require "io/console"

options = Selenium::WebDriver::Chrome::Options.new
driver = Selenium::WebDriver.for :chrome, options: options
driver.navigate.to "https://prod-web.neu.edu/wasapp/DARSStudent/RequestAuditServlet"

puts "Please sign in to myNortheastern to download your audit."
puts "Press enter here once logged in, and 2FA authenticated."
gets

driver.navigate.to "https://prod-web.neu.edu/wasapp/DARSStudent/USignInServlet?sys=UDC"

puts "Submitting new audit request."
driver.find_element(name: "Submit2").click

printf "Waiting for audit to be generated."
num_audits = driver.find_elements(name: "DETAILS").length
num_prev_audits = num_audits

while num_audits <= num_prev_audits
  sleep 2
  num_audits = driver.find_elements(name: "DETAILS").length
  printf "."
  driver.find_element(css: "[type=SUBMIT]").click
end
puts

puts "Audit ready."

driver.find_elements(name: "DETAILS")[0].click

puts "Opening printer-friendly version."
driver.find_elements(css: "table tbody tr td a")[3].click

puts "Saving as audit.html"
File.write("audit.html", driver.page_source)

puts "Done."
driver.quit
